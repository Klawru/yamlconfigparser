package ru.klaw;

import org.apache.commons.io.IOUtils;
import org.yaml.snakeyaml.reader.UnicodeReader;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.nio.channels.Channels;
import java.nio.channels.FileLock;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Objects;

public class FileReader {

    /**
     * Блокирует файл от изменений на время чтения.
     * @param path путь до файла
     * @return весь файл как строку
     */
    public static String read(String path) {
        try (RandomAccessFile file = new RandomAccessFile(new File(path), "r");) {
            final FileLock lock = file.getChannel().lock(0L, Long.MAX_VALUE, true);
            final Reader reader = Channels.newReader(file.getChannel(), StandardCharsets.UTF_8.name());
            final String result = IOUtils.toString(reader);
            lock.release();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

}
