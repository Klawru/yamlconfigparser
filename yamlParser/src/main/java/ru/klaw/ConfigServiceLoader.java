package ru.klaw;

import ru.klaw.api.ConfigExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ServiceLoader;

public class ConfigServiceLoader implements ConfigProvider {

    private final ServiceLoader<ConfigExtension> loader;
    private List<ConfigExtension> list;

    public ConfigServiceLoader() {
        loader = ServiceLoader.load(ConfigExtension.class);
        reloadConfigs();
    }

    private void reloadConfigs() {
        list = new ArrayList<>();
        loader.iterator().forEachRemaining(list::add);
        list = Collections.unmodifiableList(list);
    }

    @Override
    public List<ConfigExtension> getConfigExtensions() {
        return list;
    }

    @Override
    public void reload() {
        loader.reload();
        reloadConfigs();
    }

}
