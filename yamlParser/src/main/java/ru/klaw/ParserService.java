package ru.klaw;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.error.YAMLException;
import org.yaml.snakeyaml.representer.Representer;
import ru.klaw.api.ConfigExtension;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class ParserService {


    final List<ConfigExtension> configExtensions;


    public ParserService(ConfigProvider provider) {
        this.configExtensions = provider.getConfigExtensions();
    }


    public List<Object> parse(String file) {
        final Iterable<Object> objects = getYaml().loadAll(file);
        final List<Object> resultList = new ArrayList<>();
        for (Iterator<Object> iterator = objects.iterator(); iterator.hasNext(); ) {
            try {
                final Object next = iterator.next();
                if (next != null)
                    resultList.add(next);
            } catch (YAMLException e) {
                e.printStackTrace();
            }
        }
        return resultList;
    }

    /**
     * @return возращает подготовленный для парсинга SnakeYaml
     */
    private Yaml getYaml() {
        final Representer representer = new Representer();
        representer.getPropertyUtils().setSkipMissingProperties(true);
        final Yaml yaml = new Yaml(representer);
        configExtensions.stream()
                .map(ConfigExtension::getTypeDescriptions)
                .flatMap(Collection::stream)
                .forEach(yaml::addTypeDescription);
        return yaml;
    }


}
