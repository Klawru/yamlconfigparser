package ru.klaw.plugin;

import org.yaml.snakeyaml.TypeDescription;
import ru.klaw.api.ConfigExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AttachmentPathConfig implements ConfigExtension {

    private final List<TypeDescription> typeDescriptions;

    public AttachmentPathConfig() {
        final ArrayList<TypeDescription> list = new ArrayList<>(1);
        list.add(new TypeDescription(AttachmentPath.class, "!attachmentPath"));
        this.typeDescriptions = Collections.unmodifiableList(list);
    }

    @Override
    public List<TypeDescription> getTypeDescriptions() {
        return typeDescriptions;
    }

}
