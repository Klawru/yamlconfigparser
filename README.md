
# YamlConfigParser

Читает объекты на yaml вида:
```yaml
---
  !siebelURL
endpoint: endpoint1
login: login1
password: password1
---
  !attachmentPath  # Другой документ
localRootDir: localRootDir1
localUploadDir: localUploadDir1
remoteRootDir: remoteRootDir1
```
Пример разбора этого файла [YamlParserApplicationTests.java](https://gitlab.com/Klawru/yamlconfigparser/-/blob/master/yamlParser/src/test/java/ru/klaw/web/YamlParserApplicationTests.java)

Дополнительные документы подключают через зависимости с помощью [SPI](https://en.wikipedia.org/wiki/Service_provider_interface) и [ServiceLoader](https://gitlab.com/Klawru/yamlconfigparser/-/blob/master/yamlParser/src/main/java/ru/klaw/ConfigServiceLoader.java).

Для подключения нового документа к парсеру необходимо сделать:

 1. Добавить в зависимости [pluginApi](https://gitlab.com/Klawru/yamlconfigparser/-/tree/master/pluginApi).
 2. Создать реализацию для интерфейса ConfigExtension из pluginApi.
 3. Создать файл в META-INF\services\ru.klaw.api.ConfigExtension, и в нем указать вашу реализацию ConfigExtension .

Пример расширения для тега !siebelURL можно найти в модуле [siebelURLConfigPlugin](https://gitlab.com/Klawru/yamlconfigparser/-/tree/master/siebelURLConfigPlugin).
